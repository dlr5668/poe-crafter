UseTrans() {
	ClickRight(80, 380)
	ClickItem()
	return True	
}

UseAug() {
	ClickRight(215, 380)
	ClickItem()
	return True
}

UseAlt() {
	ClickRight(145, 380)
	ClickItem()
	return True
}

UseRegal() {
	ClickRight(215, 450)
	ClickItem()
	return True
}

UseScour() {
	ClickRight(450, 555)
	ClickItem()
	return True
}

UseAlchemy() {
	ClickRight(450, 380)
	ClickItem()
	return True
}

ClickItem() {
	ClickLeft(330, 570)
}

MouseOverItem() {
	MouseMove(330, 570)
}