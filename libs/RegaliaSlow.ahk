CraftRegaliaSlow(itemDesc) {
	esAdd := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+ to maximum Energy Shield", foundStr)
	If (foundPos > 0) {
		esAdd := StrToInt(RegExReplace(foundStr, "\D"))
	}

	esMult := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+% increased Energy Shield", foundStr)
	If (foundPos > 0) {
		esMult := StrToInt(RegExReplace(foundStr, "\D"))
	}

	stunAdd := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+% increased Stun and Block Recovery", foundStr)
	If (foundPos > 0) {
		stunAdd := StrToInt(RegExReplace(foundStr, "\D"))
	}

	bNormal := False
	foundPos := RegExMatch(itemDesc, "Rarity: Normal", foundStr)
	If (foundPos > 0) {
		bNormal := True
	}	

	bMagic := False
	foundPos := RegExMatch(itemDesc, "Rarity: Magic", foundStr)
	If (foundPos > 0) {
		bMagic := True
	}

	bRare := False
	foundPos := RegExMatch(itemDesc, "Rarity: Rare", foundStr)
	If (foundPos > 0) {
		bRare := True
	}	

	bRes := 0
	foundPos := RegExMatch(itemDesc, "Resistance", foundStr)
	If (foundPos > 0) {
		bRes := True
	}

	bInt := 0
	foundPos := RegExMatch(itemDesc, "Intelligence", foundStr)
	If (foundPos > 0) {
		bInt := True
	}	

	; White item
	If (bNormal) {
		return UseTrans()		
	}

	If (bMagic) {
		If (CanAugVaalRegalia(itemDesc)) {
			; Blue item - suffix or prefix
			If (esAdd >= 136 || esMult >= 121 || bRes || bInt) {
				return UseAug()
			}
			Else {
				return UseAlt()
			}		
		}
		Else {
			; Blue item - suffix and prefix
			If (esAdd >= 136 || esMult >= 121) {
				return UseRegal()
			}
			Else {
				return UseAlt()
			}
		}
	}

	; Yellow item
	If (bRare) {
		Sleep(1000)
		; T2+ Flat T2+ ES%
		If (esAdd >= 107 && esMult >= 101) {
			return False
		}
		; T2+ Flat T3+ Hybrid
		Else If (esAdd >= 107 && esMult >= 33 && esMult <= 56 && stunAdd >= 12) {
			return False
		}
		Else {
			return UseScour()
		}		
	}
}

CanAugVaalRegalia(itemDesc) {
	loop, parse, itemDesc, `n, `r
	max := a_index
	return (max == 16)
}