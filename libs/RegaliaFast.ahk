CraftRegaliaFast(itemDesc) {
	esAdd := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+ to maximum Energy Shield", foundStr)
	If (foundPos > 0) {
		esAdd := StrToInt(RegExReplace(foundStr, "\D"))
	}

	esMult := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+% increased Energy Shield", foundStr)
	If (foundPos > 0) {
		esMult := StrToInt(RegExReplace(foundStr, "\D"))
	}

	stunAdd := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+% increased Stun and Block Recovery", foundStr)
	If (foundPos > 0) {
		stunAdd := StrToInt(RegExReplace(foundStr, "\D"))
	}

	bNormal := False
	foundPos := RegExMatch(itemDesc, "Rarity: Normal", foundStr)
	If (foundPos > 0) {
		bNormal := True
	}

	bRare := False
	foundPos := RegExMatch(itemDesc, "Rarity: Rare", foundStr)
	If (foundPos > 0) {
		bRare := True
	}	

	bRes := 0
	foundPos := RegExMatch(itemDesc, "Resistance", foundStr)
	If (foundPos > 0) {
		bRes := True
	}

	lifeAdd := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+ to maximum Life", foundStr)
	If (foundPos > 0) {
		lifeAdd := StrToInt(RegExReplace(foundStr, "\D"))
	}

	manaAdd := 0
	foundPos := RegExMatch(itemDesc, "[0-9]+ to maximum Mana", foundStr)
	If (foundPos > 0) {
		manaAdd := StrToInt(RegExReplace(foundStr, "\D"))
	}

	; White item
	If (bNormal) {
		return UseAlchemy()	
	}

	; Yellow item
	If (bRare) {
		; T2 Flat / T3 Hybrid / T4% craft
		If (esAdd >= 107 && esMult >= 33 && esMult <= 56 && stunAdd >= 12 && BodyCanCraftPrefix(itemDesc)) {
			return False
		}
		; T3 Flat / T3 Hybrid / T3% ES
		Else If (esAdd >= 73 && esMult >= 33+83 && stunAdd >= 12) {
			return False
		}
		; T2 Flat / T2% ES / slam hybrid
		Else If (esAdd >= 107 && esMult >= 101 && BodyCanCraftPrefix(itemDesc)) {
			return False
		}
		; T2 Flat / T2% ES / T2 Life or T2 Mana
		Else If (esAdd >= 107 && esMult >= 101 && (lifeAdd >= 100 || manaAdd >= 65)) {
			return False
		}
		; T1 Flat / T2% ES
		Else If (esAdd >= 136 && esMult >= 101) {
			return False
		}
		; T2 Flat / T1% ES
		Else If (esAdd >= 107 && esMult >= 121) {
			return False
		}			
		Else {
			return UseScour()
		}		
	}
}

BodyCanCraftPrefix(itemDesc) {
	count := 0
	foundPos := RegExMatch(itemDesc, "Physical Damage to Melee Attackers", foundStr)
	If (foundPos > 0) {
		count := count + 1
	}

	foundPos := RegExMatch(itemDesc, "to maximum Energy Shield", foundStr)
	If (foundPos > 0) {
		count := count + 1
	}

	foundPos := RegExMatch(itemDesc, "increased Energy Shield", foundStr)
	If (foundPos > 0) {
		count := count + 1
	}

	foundPos := RegExMatch(itemDesc, "to maximum Life", foundStr)
	If (foundPos > 0) {
		count := count + 1
	}

	foundPos := RegExMatch(itemDesc, "to maximum Mana", foundStr)
	If (foundPos > 0) {
		count := count + 1
	}

	return (count <= 2)
}