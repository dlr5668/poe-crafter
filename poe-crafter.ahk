#Persistent
CoordMode, Mouse, Client
CoordMode, Pixel, Client
#Include %A_ScriptDir%\libs\Helpers.ahk
#Include %A_ScriptDir%\libs\OrbPosition.ahk
#Include %A_ScriptDir%\libs\RegaliaSlow.ahk
#Include %A_ScriptDir%\libs\RegaliaFast.ahk
global DO_CRAFT
return

F1::
	DO_CRAFT := True
	while (CraftItemSlow() && DO_CRAFT) {
}
return

F3::
	DO_CRAFT := True
	while (CraftItemFast() && DO_CRAFT) {
}
return

F11::
	DO_CRAFT := False
return

F12::
	ExitApp
return

; crafts item with Alt Regal
CraftItemSlow() {
	itemDesc := GetItemInfo()

	isVaalRegalia := RegExMatch(itemDesc, "Vaal Regalia", foundStr)
	If (isVaalRegalia > 0) {
		return CraftRegaliaSlow(itemDesc)
	}
	return true
}

; crafts item with Alch Scour
CraftItemFast() {
	itemDesc := GetItemInfo()

	isVaalRegalia := RegExMatch(itemDesc, "Vaal Regalia", foundStr)
	If (isVaalRegalia > 0) {
		return CraftRegaliaFast(itemDesc)
	}
	return true
}

GetItemInfo() {
	Clipboard := ""
	Sleep(100)
	MouseOverItem()
	Sleep(100)
	SendInput ^c
	Sleep(100)
	;antistuck
	If (Clipboard == "") {
		ClickItem()
	}
	Return Clipboard
}